/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.android.opengl;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import android.opengl.GLES20;

/**
 * A two-dimensional triangle for use as a drawn object in OpenGL ES 2.0.
 */
public class Cube {

    private final String vertexShaderCode =
            // This matrix member variable provides a hook to manipulate
            // the coordinates of the objects that use this vertex shader
            "uniform mat4 uMVPMatrix;" +
            "attribute vec4 vPosition;" +
            "attribute vec2 a_texCoord;" +
            "varying vec2 v_texCoord;" +
            "void main() {" +
            // the matrix must be included as a modifier of gl_Position
            // Note that the uMVPMatrix factor *must be first* in order
            // for the matrix multiplication product to be correct.
            "   v_texCoord = a_texCoord;" +
            "  gl_Position = uMVPMatrix * vPosition;" +
            "}";

    private final String fragmentShaderCode =
            "precision mediump float;" +
            "varying vec2 v_texCoord;" +
            "uniform sampler2D s_texture;" +
            "void main() {" +
            "  gl_FragColor = texture2D(s_texture, v_texCoord);" +
            "}";

    private final int mProgramObject;
    private final int mPositionLoc;
    private final int mTexCoordLoc;
    private final int mSamplerLoc;
    private final int mOffsetLoc;
    private final int mTextureId;
    private final FloatBuffer mVertices;
    private final ShortBuffer mIndices;
    private int mMVPMatrixHandle;

    // number of coordinates per vertex in this array
    static final int COORDS_PER_VERTEX = 3;
    private final int vertexStride = COORDS_PER_VERTEX * 4; // 4 bytes per vertex

    float color[] = { 0.63671875f, 0.76953125f, 0.22265625f, 0.0f };

    private final float[] mVerticesData =
            {
                    -0.6f,  0.0f, -0.4f, 1.0f,  // A0
                    -1.0f,  0.0f,             // TexCoord 0
                    0.0f, 0.6f, -0.4f, 1.0f, // B1
                    0.0f,  1.0f,              // TexCoord 1
                    0.6f, 0.0f, -0.4f, 1.0f, // C2
                    1.0f,  0.0f,               // TexCoord 2
                    0.0f, -0.6f, -0.4f, 1.0f, // D3
                    0.0f,  -1.0f,              // TexCoord 2

                    -0.6f,  0.0f, 0.4f, 1.0f,  // E4
                    1.0f,  0.0f,               // TexCoord 2
                    0.0f, 0.6f, 0.4f, 1.0f, // F5
                    0.0f,  -1.0f,              // TexCoord 2
                    0.6f, 0.0f, 0.4f, 1.0f, // G6
                    -1.0f,  0.0f,              // TexCoord 0
                    0.0f, -0.6f, 0.4f, 1.0f, // H7
                    0.0f,  1.0f,               // TexCoord 1
            };

    private final short[] mIndicesData =
            {
                    0, 1, 2,   //ABC
                    0, 2, 3,   //ACD
                    6, 5, 1,   //GFB
                    6, 1, 2,   //GBC
                    7, 6, 2,   //HGC
                    7, 2, 3,   //HCD
                    4, 7, 3,   //EHD
                    4, 3, 0,   //EDA
                    5, 4, 0,   //FEA
                    5, 0, 1,   //FAB
                    4, 5, 6,   //ABC
                    4, 6, 7    //ACD
            };

    /**
     * Sets up the drawing object data for use in an OpenGL ES context.
     */
    public Cube() {

        GLES20.glEnable( GLES20.GL_DEPTH_TEST );
        GLES20.glDepthFunc( GLES20.GL_LEQUAL );
        GLES20.glDepthMask( true );

        mVertices = ByteBuffer.allocateDirect(mVerticesData.length * 4)
                .order(ByteOrder.nativeOrder()).asFloatBuffer();
        mVertices.put(mVerticesData).position(0);
        mIndices = ByteBuffer.allocateDirect(mIndicesData.length * 2)
                .order(ByteOrder.nativeOrder()).asShortBuffer();
        mIndices.put(mIndicesData).position(0);

        // prepare shaders and OpenGL program
        int vertexShader = MyGLRenderer.loadShader(
                GLES20.GL_VERTEX_SHADER, vertexShaderCode);
        int fragmentShader = MyGLRenderer.loadShader(
                GLES20.GL_FRAGMENT_SHADER, fragmentShaderCode);

        mProgramObject = GLES20.glCreateProgram();             // create empty OpenGL Program
        GLES20.glAttachShader(mProgramObject, vertexShader);   // add the vertex shader to program
        GLES20.glAttachShader(mProgramObject, fragmentShader); // add the fragment shader to program
        GLES20.glLinkProgram(mProgramObject);                  // create OpenGL program executables

        mPositionLoc = GLES20.glGetAttribLocation(mProgramObject, "vPosition");
        mTexCoordLoc = GLES20.glGetAttribLocation(mProgramObject, "a_texCoord" );

        // Get the sampler location
        mSamplerLoc = GLES20.glGetUniformLocation ( mProgramObject, "s_texture" );

        // Get the offset location
        mOffsetLoc = GLES20.glGetUniformLocation( mProgramObject, "u_offset" );

        // Load the texture
        mTextureId = createTexture2D ();

        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

    }

    ///
    // Create a 2D texture image
    //
    private int createTexture2D( )
    {
        // Texture object handle
        int[] textureId = new int[1];
        int    width = 256,
                height = 256;
        ByteBuffer pixels;

        pixels = genCheckImage( width, height, 64 );

        // Generate a texture object
        GLES20.glGenTextures ( 1, textureId, 0 );

        // Bind the texture object
        GLES20.glBindTexture ( GLES20.GL_TEXTURE_2D, textureId[0] );

        // Load mipmap level 0
        GLES20.glTexImage2D ( GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGB, width, height,
                0, GLES20.GL_RGB, GLES20.GL_UNSIGNED_BYTE, pixels );

        // Set the filtering mode
        GLES20.glTexParameteri ( GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR );
        GLES20.glTexParameteri ( GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR );

        return textureId[0];
    }

    ///
    //  Generate an RGB8 checkerboard image
    //
    private ByteBuffer genCheckImage( int width, int height, int checkSize )
    {
        int x,
                y;
        byte[] pixels = new byte[width * height * 3];


        for ( y = 0; y < height; y++ )
            for ( x = 0; x < width; x++ )
            {
                byte rColor = 0;
                byte bColor = 0;

                if ( ( x / checkSize ) % 2 == 0 )
                {
                    rColor = (byte)(127 * ( ( y / checkSize ) % 2 ));
                    bColor = (byte)(127 * ( 1 - ( ( y / checkSize ) % 2 ) ));
                }
                else
                {
                    bColor = (byte)(127 * ( ( y / checkSize ) % 2 ));
                    rColor = (byte)(127 * ( 1 - ( ( y / checkSize ) % 2 ) ));
                }

                pixels[(y * height + x) * 3] = rColor;
                pixels[(y * height + x) * 3 + 1] = 0;
                pixels[(y * height + x) * 3 + 2] = bColor;
            }

        ByteBuffer result = ByteBuffer.allocateDirect(width*height*3);
        result.put(pixels).position(0);
        return result;
    }

    /**
     * Encapsulates the OpenGL ES instructions for drawing this shape.
     *
     * @param mvpMatrix - The Model View Project matrix in which to draw
     * this shape.
     */
    public void draw(float[] mvpMatrix) {
        // Add program to OpenGL environment
        GLES20.glUseProgram(mProgramObject);

        // get handle to vertex shader's vPosition member

        mVertices.position(0);
        GLES20.glVertexAttribPointer ( mPositionLoc, 4, GLES20.GL_FLOAT,
                false,
                6 * 4, mVertices );
        // Load the texture coordinate
        mVertices.position(4);
        GLES20.glVertexAttribPointer ( mTexCoordLoc, 2, GLES20.GL_FLOAT,
                false,
                6 * 4,
                mVertices );

        // Enable a handle to the triangle vertices
        GLES20.glEnableVertexAttribArray(mPositionLoc);
        GLES20.glEnableVertexAttribArray(mTexCoordLoc);

        // Bind the texture
        GLES20.glActiveTexture ( GLES20.GL_TEXTURE0 );
        GLES20.glBindTexture ( GLES20.GL_TEXTURE_2D, mTextureId );

        // Set the sampler texture unit to 0
        GLES20.glUniform1i ( mSamplerLoc, 0 );

        // Draw quad with repeat wrap mode
        GLES20.glTexParameteri ( GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_REPEAT );
        GLES20.glTexParameteri ( GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_REPEAT );
        GLES20.glUniform1f ( mOffsetLoc, -0.7f );
        GLES20.glDrawElements ( GLES20.GL_TRIANGLES, 36, GLES20.GL_UNSIGNED_SHORT, mIndices );

        // get handle to shape's transformation matrix
        mMVPMatrixHandle = GLES20.glGetUniformLocation(mProgramObject, "uMVPMatrix");
        MyGLRenderer.checkGlError("glGetUniformLocation");

        // Apply the projection and view transformation
        GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, mvpMatrix, 0);
        MyGLRenderer.checkGlError("glUniformMatrix4fv");




    }

}
